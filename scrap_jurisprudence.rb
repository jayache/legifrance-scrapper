require 'open-uri'
require 'to_regexp'
require 'rubygems'
require 'nokogiri'
require 'mechanize'

BASEURL = "https://www.legifrance.gouv.fr"
BASENAME = "juris/"
TARGET = "CHAMBRE_SOCIALE"

def handle_page(a, page, i, name)
	puts "Scrapping [#{page.title.strip}] as [#{name}]"
	File.open(name, "w") {|file| file.write(page.body) }
end

def handle_listpage(a, results, i)
	puts results.title
	cResult = results
	cResult.links_with(text: /Cour de cassation/i).each do |link|
		name = BASENAME + "#{link.text[0..200]}"
		if (File.file?(name))
			puts "This file already exists -- skipping"
			next
		end
		a.get(link.click) do |result|
			handle_page(a, result, i, name)
			i += 1
		end
	end
	i
end

a = Mechanize.new { |agent|
	agent.user_agent_alias = 'Mac Safari'
}

a.get(BASEURL) do |page|
	a.get(a.click("judiciaire")) do |form_form|
		a.get(form_form.link(text: /Recherche/).click) do |form|
			f = form.forms[0]
			f.checkbox_with(name: /checkboxRechercherCassation/).check
			f.checkbox_with(name: /checkboxDecisionsPubliees/).check
			f.checkbox_with(name: /checkboxDecisionsNonPubliees/).check
			f["champFormation"] = TARGET
			results = a.get(f.submit)
			pagenumber = 1;
			estimated = 133779 / 20
			i = 0
			while true	
				i = handle_listpage(a, results, i);
				pagenumber += 1
				puts "We are at page #{pagenumber}/#{estimated}"
				results = a.get(results.link_with(:text => "/#{pagenumber}/".to_regexp).click)
			end
		end
	end
end
